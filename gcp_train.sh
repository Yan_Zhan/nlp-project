#!/bin/bash

source ~/.bashrc
if [ ! -d $HOME/nltk_data ]; then
    python -c 'import nltk; nltk.download("punkt")'
fi

if [ ! -d nlp ]; then
    mkdir nlp
fi

src="$1"
tgt="$2"
tok="$3"
epochs="$4"
batch="$5"
output_path="$6"
nts="$7"
if [ -z ${nts} ] || [ ${nts} == "" ]; then
    nts=$(cat /proc/cpuinfo | grep processor | wc -l)
fi

python train.py ${src}-${tgt}-train.txt ${src}-${tgt}-val.txt ${src}-${tgt}-test.txt ${epochs} ${batch} ${tok} data/${src}-${tgt}/${src}-tok.txt data/${src}-${tgt}/${tgt}-tok.txt --data_path data/${src}-${tgt} --d_model 512 --nhead 8 --num_encoder_layers 6 --num_decoder_layers 6 --learning_rate 0.0001 --dropout 0.1 --checkpoint_file ${output_path}/${src}-${tgt}-${tok}.ck --log_file ${output_path}/${src}-${tgt}-${tok}.log --use_gpu False --num_threads ${nts}
