#!/usr/bin/env bash

sudo apt update && \
sudo apt upgrade -y

pushd /tmp && \
curl -O https://repo.anaconda.com/archive/Anaconda3-2020.07-Linux-x86_64.sh && \
sudo bash Anaconda3-2020.07-Linux-x86_64.sh -b -p /opt/conda && \
rm Anaconda3-2020.07-Linux-x86_64.sh && \
sudo ln -s /opt/conda/etc/profile.d/conda.sh /etc/profile.d/conda.sh && \
echo ". /opt/conda/etc/profile.d/conda.sh" >> ~/.bashrc && \
echo "conda activate base" >> ~/.bashrc && \
popd

sudo /opt/conda/bin/conda install torchtext nltk pytorch torchvision torchaudio cpuonly -c pytorch -y && \
sudo /opt/conda/bin/pip install tokenizers
